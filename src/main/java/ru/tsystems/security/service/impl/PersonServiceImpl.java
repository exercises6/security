package ru.tsystems.security.service.impl;

import org.springframework.stereotype.Service;
import ru.tsystems.security.dao.PersonDao;
import ru.tsystems.security.model.Person;
import ru.tsystems.security.service.PersonService;

@Service
public class PersonServiceImpl implements PersonService {
    private final PersonDao personDao;

    public PersonServiceImpl(PersonDao personDao) {
        this.personDao = personDao;
    }

    @Override
    public Person findByLogin(String login) {
        return personDao.findByLogin(login);
    }
}
