package ru.tsystems.security.service;

import ru.tsystems.security.model.Person;

public interface PersonService {
    Person findByLogin(String login);
}
