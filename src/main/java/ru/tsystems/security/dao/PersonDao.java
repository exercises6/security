package ru.tsystems.security.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.tsystems.security.model.Person;

public interface PersonDao extends JpaRepository<Person, Long> {
    Person findByLogin(String login);
}
